// Copyright 2023 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package gitea

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"
)

// PushMirror represents a push mirror for a repository
type PushMirror struct {
	Created       time.Time  `json:"created"`
	Interval      string     `json:"interval"`
	LastError     string     `json:"last_error"`
	LastUpdate    *time.Time `json:"last_update"`
	RemoteAddress string     `json:"remote_address"`
	RemoteName    string     `json:"remote_name"`
	RepoName      string     `json:"repo_name"`
	SyncOnCommit  bool       `json:"sync_on_commit"`
}

// ListPushMirrorsOptions options for listing push mirrors
type ListPushMirrorsOptions struct {
	ListOptions
}

func (c *Client) ListPushMirrors(owner, repo string, opt ListPushMirrorsOptions) ([]*PushMirror, *Response, error) {
	opt.setDefaults()
	pushMirrors := make([]*PushMirror, 0, opt.PageSize)
	resp, err := c.getParsedResponse("GET", fmt.Sprintf("/repos/%s/%s/push_mirrors?%s", owner, repo, opt.getURLQuery().Encode()), nil, nil, &pushMirrors)
	return pushMirrors, resp, err
}

// AddPushMirrorOption options when adding a push mirror to a repository
type AddPushMirrorOption struct {
	// set to a string like `8h30m0s` to set the mirror interval time
	Interval string `json:"interval,omitempty"`
	// the remote address to mirror the repository to
	RemoteAddress string `json:"remote_address,omitempty"`
	// the remote password to authenticate against the mirror
	RemotePassword string `json:"remote_password,omitempty"`
	// the remote username to authenticate against the mirror
	RemoteUsername string `json:"remote_username,omitempty"`
	// set to true to sync on commit
	SyncOnCommit bool `json:"sync_on_commit,omitempty"`
}

// AddPushMirror adds a push mirror to a repository
func (c *Client) AddPushMirror(owner, repo string, opt AddPushMirrorOption) (*PushMirror, *Response, error) {
	body, err := json.Marshal(&opt)
	if err != nil {
		return nil, nil, err
	}
	pushMirror := new(PushMirror)
	resp, err := c.getParsedResponse("POST", fmt.Sprintf("/repos/%s/%s/push_mirrors", owner, repo), jsonHeader, bytes.NewReader(body), pushMirror)
	return pushMirror, resp, err
}

// SyncPushMirrors syncs all push mirrors for a repository
func (c *Client) SyncPushMirrors(owner, repo string) (*Response, error) {
	_, resp, err := c.getResponse("POST", fmt.Sprintf("/repos/%s/%s/push_mirrors-sync", owner, repo), nil, nil)
	return resp, err
}

// GetPushMirror gets a push mirror by its remote name
func (c *Client) GetPushMirror(owner, repo, name string) (*PushMirror, *Response, error) {
	pushMirror := new(PushMirror)
	resp, err := c.getParsedResponse("GET", fmt.Sprintf("/repos/%s/%s/push_mirrors/%s", owner, repo, name), nil, nil, pushMirror)
	if err != nil {
		return nil, resp, err
	}

	return pushMirror, resp, err
}

// DeletePushMirror deletes a push mirror by its remote name
func (c *Client) DeletePushMirror(owner, repo, name string) (*Response, error) {
	_, resp, err := c.getResponse("DELETE", fmt.Sprintf("/repos/%s/%s/push_mirrors/%s", owner, repo, name), nil, nil)
	return resp, err
}
