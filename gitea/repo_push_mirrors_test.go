// Copyright 2023 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package gitea

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddPushMirror(t *testing.T) {
	log.Println("== TestAddPushMirror ==")
	c := newTestClient()

	repo, err := createTestRepo(t, "TestAddPushMirror", c)
	assert.NoError(t, err)

	pushMirror, _, err := c.AddPushMirror(repo.Owner.UserName, repo.Name, AddPushMirrorOption{
		RemoteAddress: "https://gitea.com/gitea/go-sdk.git",
		Interval:      "8h30m0s",
	})
	assert.NoError(t, err)
	assert.NotNil(t, pushMirror)
}

func TestDeletePushMirror(t *testing.T) {
	log.Println("== TestDeletePushMirror ==")
	c := newTestClient()

	repo, err := createTestRepo(t, "TestDeletePushMirror", c)
	assert.NoError(t, err)

	pushMirror, _, err := c.AddPushMirror(repo.Owner.UserName, repo.Name, AddPushMirrorOption{
		RemoteAddress: "https://gitea.com/gitea/go-sdk.git",
		Interval:      "8h30m0s",
	})
	assert.NoError(t, err)
	assert.NotNil(t, pushMirror)

	pushMirror, _, err = c.GetPushMirror(repo.Owner.UserName, repo.Name, pushMirror.RemoteName)
	assert.NoError(t, err)
	assert.NotNil(t, pushMirror)

	_, err = c.DeletePushMirror(repo.Owner.UserName, repo.Name, pushMirror.RemoteName)
	assert.NoError(t, err)

	pushMirror, _, err = c.GetPushMirror(repo.Owner.UserName, repo.Name, "ABCD")
	assert.Error(t, err)
	assert.Nil(t, pushMirror)
}

func TestGetPushMirror(t *testing.T) {
	log.Println("== TestGetPushMirror ==")
	c := newTestClient()

	repo, err := createTestRepo(t, "TestGetPushMirror", c)
	assert.NoError(t, err)

	pushMirror, _, err := c.GetPushMirror(repo.Owner.UserName, repo.Name, "not-exist")
	assert.Error(t, err)
	assert.Nil(t, pushMirror)

	pushMirror, _, err = c.AddPushMirror(repo.Owner.UserName, repo.Name, AddPushMirrorOption{
		RemoteAddress: "https://gitea.com/gitea/go-sdk.git",
		Interval:      "8h30m0s",
	})
	assert.NoError(t, err)
	assert.NotNil(t, pushMirror)

	pushMirror, _, err = c.GetPushMirror(repo.Owner.UserName, repo.Name, pushMirror.RemoteName)
	assert.NoError(t, err)
	assert.NotNil(t, pushMirror)
}

func TestListPushMirrors(t *testing.T) {
	log.Println("== TestListPushMirrors ==")
	c := newTestClient()

	repo, err := createTestRepo(t, "TestListPushMirrors", c)
	assert.NoError(t, err)

	pushMirrors, _, err := c.ListPushMirrors(repo.Owner.UserName, repo.Name, ListPushMirrorsOptions{})
	assert.NoError(t, err)
	assert.Len(t, pushMirrors, 0)

	_, _, err = c.AddPushMirror(repo.Owner.UserName, repo.Name, AddPushMirrorOption{
		RemoteAddress: "https://gitea.com/gitea/go-sdk.git",
		Interval:      "8h30m0s",
	})
	assert.NoError(t, err)

	pushMirrors, _, err = c.ListPushMirrors(repo.Owner.UserName, repo.Name, ListPushMirrorsOptions{})
	assert.NoError(t, err)
	assert.Len(t, pushMirrors, 1)

	_, _, err = c.AddPushMirror(repo.Owner.UserName, repo.Name, AddPushMirrorOption{
		RemoteAddress: "https://gitea.com/gitea/go-sdk.git",
		Interval:      "8h30m0s",
	})
	assert.NoError(t, err)

	pushMirrors, _, err = c.ListPushMirrors(repo.Owner.UserName, repo.Name, ListPushMirrorsOptions{})
	assert.NoError(t, err)
	assert.Len(t, pushMirrors, 2)
}
